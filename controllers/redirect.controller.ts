import { Response, Request } from 'express';
import { response } from '../server';

class RedirectController {
  public static async toAuth(req: Request, res: Response): Promise<response> {
    try {
      res.redirect('/user/login');
    } catch (e) {
      res.redirect('/something-went-wrong');
      console.log(e);
    }
  }

  public static async notFound(req: Request, res: Response): Promise<response> {
    try {
      res.redirect('/something-went-wrong');
    } catch (e) {
      res.redirect('/something-went-wrong');
      console.log(e);
    }
  }
}

export default RedirectController;
