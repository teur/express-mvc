import { Response, Request } from 'express';
import { response } from '../server';
import UserModel, { IUser } from '../models/user.model';
import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcrypt';

class UserController {
  public static loginGet(req: Request, res: Response): void {
    res.render('auth', {
      title: 'Login',
      isLogin: true,
    });
  }

  public static registerGet(req: Request, res: Response): void {
    res.render('auth', {
      title: 'Register',
      isLogin: false,
    });
  }

  public static async loginPost(
    req: Request,
    res: Response
  ): Promise<response> {
    try {
      const { username, password } = req.body;
      const candidate: IUser = await UserModel.findOne({ username });

      if (!candidate)
        return res
          .status(404)
          .json({ msg: "Can't find user with the same username!" });

      const isPasswordCorrect: boolean = await bcrypt.compare(
        password,
        candidate.password
      );

      if (!isPasswordCorrect)
        return res
          .status(404)
          .json({ msg: 'Wrong password or username!', code: 404 });

      const token: string = jwt.sign(username, 'this app', {
        expiresIn: '8h',
      });

      return res.json({ username, token, code: 201 });
    } catch (e) {
      res.redirect('/something-went-wrong');
      console.log(e);
    }
  }

  public static async registerPost(
    req: Request,
    res: Response
  ): Promise<response> {
    try {
      const { username, password } = req.body;

      if (!username || !password)
        return res.status(400).json({ msg: 'no username or password' });

      const candidate: IUser = await UserModel.findOne({ username });

      if (candidate)
        return res.status(400).json({
          msg: 'User with same username is already registred!',
          code: 400,
        });

      const user: IUser = await UserModel.create({ username, password });
      await user.save();

      return res.status(201).json({ msg: 'user created', code: 201 });
    } catch (e) {
      res.redirect('/something-went-wrong');
      console.log(e);
    }
  }
}

export default UserController;
