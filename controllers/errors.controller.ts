import { Response, Request } from 'express';
import { response } from '../server';

class ErrorsController {
  public static notFound(req: Request, res: Response): void {
    res.status(400).render('404');
  }

  public static internal(req: Request, res: Response): void {
    res.status(500).render('internal');
  }
}

export default ErrorsController;
