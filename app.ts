import { Server } from './server';
import * as dotenv from 'dotenv';
import * as path from 'path';
import UserRoutes from './routes/user.routes';
import RedirectRoutes from './routes/redirect.routes';
import ErrorsRoutes from './routes/errors.routes';

dotenv.config({ path: path.join(__dirname, '.env') });

Server.getInstance({
  port: 3000,
  routes: [UserRoutes, RedirectRoutes, ErrorsRoutes],
  db: {
    uri: process.env.DB_URI,
  },
}).listen();
