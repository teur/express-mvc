const path = require('path');

module.exports = {
  entry: path.join(__dirname, 'public', 'index.js'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'app.js',
  }
};