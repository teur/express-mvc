import * as express from 'express';
import * as exhbs from 'express-handlebars';
import * as path from 'path';
import * as mongoose from 'mongoose';
import { Application, Router, Response } from 'express';

export type response = void | Response;

export interface IRoute {
  router: Router;
  path: string;
}

export interface IServerConfig {
  port: number;
  routes?: IRoute[];
  db?: IdbConfig;
}

interface IdbConfig {
  uri: string;
}

export class Server {
  private static instance: Server;
  private app: Application;
  private port: number;
  private db_uri: string;

  private constructor(config: IServerConfig) {
    this.app = express();
    this.port = config.port;

    if (config.db) this.db_uri = config.db.uri;

    const hbs = exhbs.create({
      defaultLayout: 'main',
      extname: 'hbs',
    });

    this.app.engine('hbs', hbs.engine);
    this.app.set('view engine', 'hbs');
    this.app.set('views', path.join(__dirname, 'views'));
    this.app.use(express.json());
    this.app.use(express.static('dist'));

    if (config.routes)
      config.routes.forEach((route) => this.applyRoutes(route));
  }

  public static getInstance(config: IServerConfig): Server {
    if (!Server.instance) Server.instance = new Server(config);

    return Server.instance;
  }

  private applyRoutes(config: IRoute): void {
    this.app.use(config.path, config.router);
  }

  public async listen(): Promise<void> {
    try {
      if (this.db_uri)
        await mongoose.connect(this.db_uri, {
          useCreateIndex: true,
          useUnifiedTopology: true,
          useNewUrlParser: true,
        });

      this.app.listen(this.port, (): void =>
        console.log(`Server has been started at ${this.port}`)
      );
    } catch (e) {
      console.log(e);
    }
  }
}
