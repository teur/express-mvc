import { Router } from 'express';
import ErrorsController from '../controllers/errors.controller';

const router: Router = Router();
enum RedirectRoutes {
  notFound = '/404',
  internal = '/something-went-wrong',
}

router.get(RedirectRoutes.notFound, ErrorsController.notFound);
router.get(RedirectRoutes.internal, ErrorsController.internal);

export default {
  router,
  path: '/',
};
