import { Router } from 'express';
import UserController from '../controllers/user.controller';

const router: Router = Router();
enum UserRoutes {
  login = '/login',
  register = '/register',
}

router.post(UserRoutes.login, UserController.loginPost);
router.get(UserRoutes.login, UserController.loginGet);
router.post(UserRoutes.register, UserController.registerPost);
router.get(UserRoutes.register, UserController.registerGet);

export default {
  router,
  path: '/user',
};
