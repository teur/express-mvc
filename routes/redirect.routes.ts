import { Router } from 'express';
import RedirectController from '../controllers/redirect.controller';

const router: Router = Router();
enum RedirectRoutes {
  main = '/',
}

router.get(RedirectRoutes.main, RedirectController.toAuth);

export default {
  router,
  path: '/',
};
