const add = (a: number, b: number) => a + b;
const dec = (a: number, b: number) => a - b;

test('jest should be works correcty;', () => {
  expect(1).toBe(1);
  expect(0).toEqual(0);
  expect(true).toBeTruthy();
});

test('should be works corrctly with funcitons', () => {
  expect(add(1, 2)).toBe(3);
  expect(dec(1, 2)).toBe(-1);
});
